param ()

$ErrorActionPreference = "Stop"
    

function main ()
{
    param ()

    $curLoc = Get-Location

    $exe_path  = $curLoc.Path + "\Debug\Tests.exe"

    $exe_args  = ""

    $output = "Executing " + $exe_path + $exe_args
    Write-Host "$output"
    Write-Host " "
    
    $proc = launchExe "$exe_path" "$exe_args"
        
    EXIT $proc.ExitCode
}

function launchExe([string]$exePath, [string]$exeArgs)
{
    $curLoc = Get-Location

    $pinfo = New-Object System.Diagnostics.ProcessStartInfo
    $pinfo.FileName = $exePath
    $pinfo.WorkingDirectory = $curLoc
    #$pinfo.RedirectStandardOutput = $true   #Don't redirect output
    #$pinfo.RedirectStandardError = $true    #Don't redirect output
    $pinfo.UseShellExecute = $false
    $pinfo.Arguments = $exeArgs
    #$pinfo.CreateNoWindow = $true

    $proc = New-Object System.Diagnostics.Process
    $proc.StartInfo = $pinfo
    $proc.Start() | Out-Null
    $proc.WaitForExit()
    
    return $proc
}

#entry point
main
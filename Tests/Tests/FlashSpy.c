#include <string.h>
#include "FlashSpy.h"
#include <stdio.h>

static uint8_t * flashMemory = NULL;


Status Flash_Init(void)
{
    flashMemory = (uint8_t*)malloc(FLASH_SIZE);
    if (flashMemory == NULL)
    {
        return FAIL;
    }
    // �������, ��� flash �������������� ����� (��� ���� ����� 0b1)
    memset(flashMemory, 0xFF, FLASH_SIZE);
    return OK;
}

Status Flash_DeInit(void)
{
    if (flashMemory == NULL)
    {
        return OK;
    }
    free(flashMemory);
    flashMemory = NULL;
    return OK;
}

Status FlashSpy_GetFlashPtr(uint32_t ** flashMemoryPtr, uint32_t address)
{
    if (flashMemory == NULL)
    {
        return WRONG_CONDITION;
    }
    if (flashMemoryPtr == NULL || address >= FLASH_SIZE)
    {
        return INVALID_PARAMETERS;
    }
    
    *flashMemoryPtr = (uint32_t*)&flashMemory[address];
    return OK;
}

Status Flash_Write(uint32_t address, uint32_t data)
{
    if (flashMemory == NULL)
    {
        return WRONG_CONDITION;
    }
    if (address >= FLASH_SIZE)
    {
        return INVALID_PARAMETERS;
    }
    // ����� �������� ���� �� �������� 0b1 � 0b0, ������� ������ � ������� Flash_Erase
    *(uint32_t*)(flashMemory + address) &= data;
    return OK;
}

Status Flash_Read(uint32_t address, uint32_t * dataPtr)
{
    if (flashMemory == NULL || dataPtr == NULL)
    {
        return WRONG_CONDITION;
    }
    if (address >= FLASH_SIZE)
    {
        return INVALID_PARAMETERS;
    }

    *dataPtr = *(uint32_t*)(flashMemory + address);
    return OK;
}

Status Flash_Erase(uint8_t pageNumber)
{
    if (flashMemory == NULL)
    {
        return WRONG_CONDITION;
    }
    if (pageNumber >= FLASH_PAGE_COUNT)
    {
        return INVALID_PARAMETERS;
    }

    uint32_t offset = pageNumber * FLASH_PAGE_SIZE;
    memset(flashMemory + offset, 0xFF, FLASH_PAGE_SIZE);
    return OK;
}
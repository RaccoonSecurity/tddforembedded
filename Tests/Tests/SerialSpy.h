#include "Common.h"
#include "Serial.h"


Status SerialSpy_GetReceiveBuffer(Serial * self, char ** bufferPtr);

Status SerialSpy_SetReceiveBuffer(Serial * self, char * data, uint32_t len);

Status SerialSpy_GetSendBuffer(Serial * self, char ** bufferPtr);
#include "CppUTest\TestHarness.h"

extern "C"
{
#include "FlashSpy.h"
}

#define FLASH_START_OFFSET 0

TEST_GROUP(FlashSpy)
{
    void setup()
    {
        LONGS_EQUAL(OK, Flash_Init());
    }

    void teardown()
    {
        Flash_DeInit();
    }
};

TEST(FlashSpy, ShouldReturnFlashPtr)
{
    // Arrange
    uint32_t * flashPtr = NULL;

    // Act
    Status status = FlashSpy_GetFlashPtr(&flashPtr, FLASH_START_OFFSET);

    // Assert
    LONGS_EQUAL(OK, status);
    CHECK_TRUE(flashPtr != NULL);
}

TEST(FlashSpy, ShouldWriteToFlash)
{
    // Arrange
    uint32_t expectedData = 0xC0FFEE;

    uint32_t * flashPtr = NULL;
    LONGS_EQUAL(OK, FlashSpy_GetFlashPtr(&flashPtr, FLASH_START_OFFSET));

    // Act
    Status status = Flash_Write(FLASH_START_OFFSET, expectedData);

    // Assert
    LONGS_EQUAL(OK, status);
    MEMCMP_EQUAL(&expectedData, flashPtr, sizeof(uint32_t));
}

TEST(FlashSpy, ShouldReadFromFlash)
{
    // Arrange
    uint32_t expectedData = 0xFEEDFACE;
    uint32_t data = 0;

    uint32_t * flashPtr = NULL;
    LONGS_EQUAL(OK, FlashSpy_GetFlashPtr(&flashPtr, FLASH_START_OFFSET));

    LONGS_EQUAL(OK, Flash_Write(FLASH_START_OFFSET + 0x100, expectedData));

    // Act
    Status status = Flash_Read(FLASH_START_OFFSET + 0x100, &data);

    // Assert
    LONGS_EQUAL(OK, status);
    LONGS_EQUAL(expectedData, data);
}

TEST(FlashSpy, ShouldEraseFlashPage)
{
    // Arrange
    uint8_t expectedFlashMemory[FLASH_PAGE_SIZE] = "";
    memset(expectedFlashMemory, 0xFF, FLASH_PAGE_SIZE);

    uint32_t * flashPtr = NULL;
    LONGS_EQUAL(OK, FlashSpy_GetFlashPtr(&flashPtr, FLASH_START_OFFSET + 0x2000));
    memset(flashPtr, 0, FLASH_PAGE_SIZE);

    // Act
    Status status = Flash_Erase(8);

    // Assert
    LONGS_EQUAL(OK, status);
    MEMCMP_EQUAL(expectedFlashMemory, flashPtr, FLASH_PAGE_SIZE);
}
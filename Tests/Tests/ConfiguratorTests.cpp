#include "CppUTest/TestHarness.h"

extern "C"
{
#include "Configurator.h"
#include "SerialSpy.h"
#include "FlashSpy.h"
}


TEST_GROUP(Configurator)
{
    Configurator * configurator = NULL;
    Serial * serial = NULL;

    void setup()
    {
        serial = Serial_Create();
        configurator = Configurator_Create(serial);
        Flash_Init();
    }
    void teardown()
    {
        Configurator_Destroy(configurator);
        Flash_DeInit();
    }
};

TEST(Configurator, ShouldNotBeNull)
{
    CHECK_TRUE(configurator);
}

TEST(Configurator, ShouldNotHandleUnknownCommand)
{
    // Arrange 
    char unknownCommand[] = "Unknown command\r\n";
    Status status = SerialSpy_SetReceiveBuffer(serial, unknownCommand, sizeof(unknownCommand));
    LONGS_EQUAL(OK, status);

    // Act
    status = Configurator_Handler(configurator);

    // Assert
    LONGS_EQUAL(UNSUPPORTED, status);
}

TEST(Configurator, ShouldHandleHelpCommand)
{
    // Arrange 
    char helpCommand[] = "help\r\n";
    LONGS_EQUAL(OK, SerialSpy_SetReceiveBuffer(serial, helpCommand, sizeof(helpCommand)));
    
    char * sendBufferPtr = NULL;
    LONGS_EQUAL(OK, SerialSpy_GetSendBuffer(serial, &sendBufferPtr));

    // Act
    Status status = Configurator_Handler(configurator);

    // Assert
    LONGS_EQUAL(OK, status);
    STRCMP_EQUAL(HELP_OUTPUT, sendBufferPtr);
}

TEST(Configurator, ShouldHandleWriteFlashCommand)
{
    // Arrange 
    // ��������� ������ ����� ��������� ������� write
    uint32_t expectedFlashData = 0x11223344;
    // ������������� �������� ������� �� �� � ����� ����� UART
    char writeFlashCommand[] = "write: 0x10000 0x11223344\r\n";
    LONGS_EQUAL(OK, SerialSpy_SetReceiveBuffer(serial, writeFlashCommand, sizeof(writeFlashCommand)));

    // Act
    Status status = Configurator_Handler(configurator);

    // Assert
    // ��������� ������
    LONGS_EQUAL(OK, status);
    // � ��������� ��������� ������ �� Flash
    uint32_t * flashPtr = NULL;
    LONGS_EQUAL(OK, FlashSpy_GetFlashPtr(&flashPtr, 0x10000));
    LONGS_EQUAL(expectedFlashData, *flashPtr);
}

TEST(Configurator, ShouldHandleReadFlashCommand)
{
    // Arrange 
    char expectedSerialResponse[] = "data: 0xCC00FFEE\r\n>";

    char readFlashCommand[] = "read: 0x1A000\r\n";
    LONGS_EQUAL(OK, SerialSpy_SetReceiveBuffer(serial, readFlashCommand, sizeof(readFlashCommand)));

    char * sendBufferPtr = NULL;
    LONGS_EQUAL(OK, SerialSpy_GetSendBuffer(serial, &sendBufferPtr));

    LONGS_EQUAL(OK, Flash_Write(0x1A000, 0xCC00FFEE));

    // Act
    Status status = Configurator_Handler(configurator);

    // Assert
    LONGS_EQUAL(OK, status);
    STRCMP_EQUAL(expectedSerialResponse, sendBufferPtr);
}

TEST(Configurator, ShouldHandleEraseFlashCommand)
{
    // Arrange 
    uint8_t expectedFlashMemory[FLASH_PAGE_SIZE] = "";
    memset(expectedFlashMemory, 0xFF, FLASH_PAGE_SIZE);

    char eraseFlashCommand[] = "erase: 0x10\r\n";
    LONGS_EQUAL(OK, SerialSpy_SetReceiveBuffer(serial, eraseFlashCommand, sizeof(eraseFlashCommand)));

    uint32_t * flashPtr = NULL;
    LONGS_EQUAL(OK, FlashSpy_GetFlashPtr(&flashPtr, 0x4000));
    memset(flashPtr, 0, FLASH_PAGE_SIZE);

    // Act
    Status status = Configurator_Handler(configurator);

    // Assert
    LONGS_EQUAL(OK, status);
    MEMCMP_EQUAL(expectedFlashMemory, flashPtr, FLASH_PAGE_SIZE);
}

#include "CppUTest/TestHarness.h"

extern "C"
{
#include "SerialSpy.h"
}

TEST_GROUP(Serial)
{
    Serial * serial = NULL;

    void setup()
    {
        serial = Serial_Create();
    }
    void teardown()
    {
        Serial_Destroy(serial);
    }
};

TEST(Serial, ShouldNotBeNull)
{
    CHECK_TRUE(serial);
}

TEST(Serial, ShouldBeEmptyReceiveBufferAfterCreation)
{
    // Arrange 
    char * serialBufferPtr = NULL;

    // Act
    Status status = SerialSpy_GetReceiveBuffer(serial, &serialBufferPtr);
    
    // Assert
    LONGS_EQUAL(OK, status);

    char expectedSerialBuffer[SERIAL_RECEIVE_BUFFER_SIZE] = "";
    MEMCMP_EQUAL(expectedSerialBuffer, serialBufferPtr, SERIAL_RECEIVE_BUFFER_SIZE);
}

TEST(Serial, ShouldSetReceiveBuffer)
{
    // Arrange
    char expectedData[SERIAL_RECEIVE_BUFFER_SIZE] = "Hello, habr";
    char * serialBufferPtr = NULL;

    // Act
    Status status = SerialSpy_SetReceiveBuffer(serial, expectedData, sizeof(expectedData));

    // Assert
    LONGS_EQUAL(OK, status);

    LONGS_EQUAL(OK, SerialSpy_GetReceiveBuffer(serial, &serialBufferPtr));
    
    MEMCMP_EQUAL(expectedData, serialBufferPtr, SERIAL_RECEIVE_BUFFER_SIZE);
}

TEST(Serial, ShouldReturnNoDataWhenReceiveBufferEmpty)
{
    // Arrange 
    char commandPtr[SERIAL_RECEIVE_BUFFER_SIZE] = "";
        
    // Act
    Status status = Serial_ReceiveCommand(serial, commandPtr);

    // Assert
    LONGS_EQUAL(NO_DATA, status);
}

TEST(Serial, ShouldReturnNoDataWhenNoNewLine)
{
    // Arrange
    char commandPtr[SERIAL_RECEIVE_BUFFER_SIZE] = "";
    char expectedCommand[SERIAL_RECEIVE_BUFFER_SIZE] = "No new line";
    Status status = SerialSpy_SetReceiveBuffer(serial, expectedCommand, sizeof(expectedCommand));

    // Act
    status = Serial_ReceiveCommand(serial, commandPtr);

    // Assert
    LONGS_EQUAL(NO_DATA, status);
}

TEST(Serial, ShouldReceiveCommandString)
{
    // Arrange
    char commandPtr[SERIAL_RECEIVE_BUFFER_SIZE] = "";
    char expectedCommand[SERIAL_RECEIVE_BUFFER_SIZE] = "Command\r\n";
    Status status = SerialSpy_SetReceiveBuffer(serial, expectedCommand, sizeof(expectedCommand));

    // Act
    status = Serial_ReceiveCommand(serial, commandPtr);

    // Assert
    LONGS_EQUAL(OK, status);
    STRCMP_EQUAL(expectedCommand, commandPtr);
}

TEST(Serial, ShouldBeEmptySendBufferAfterCreation)
{
    // Arrange 
    char * serialBufferPtr = NULL;

    // Act
    Status status = SerialSpy_GetSendBuffer(serial, &serialBufferPtr);

    // Assert
    LONGS_EQUAL(OK, status);

    char expectedSerialBuffer[SERIAL_RECEIVE_BUFFER_SIZE] = "";
    MEMCMP_EQUAL(expectedSerialBuffer, serialBufferPtr, SERIAL_RECEIVE_BUFFER_SIZE);
}

TEST(Serial, ShouldSendResponseToSendBuffer)
{
    // Arrange 
    char expectedData[SERIAL_SEND_BUFFER_SIZE] = "Data to send\r\n";

    // Act
    Status status = Serial_SendResponse(serial, expectedData);

    // Assert
    char * serialBufferPtr = NULL;;
    LONGS_EQUAL(OK, SerialSpy_GetSendBuffer(serial, &serialBufferPtr));
    MEMCMP_EQUAL(expectedData, serialBufferPtr, SERIAL_SEND_BUFFER_SIZE);
}

TEST(Serial, ShouldClearSendBuffer)
{
    // Arrange 
    char expectedData[SERIAL_RECEIVE_BUFFER_SIZE] = "";
    char receivedData[SERIAL_RECEIVE_BUFFER_SIZE] = "Received data\r\n";
    LONGS_EQUAL(OK, SerialSpy_SetReceiveBuffer(serial, receivedData, sizeof(receivedData)));

    // Act
    Status status = Serial_Clear(serial);

    // Assert
    char * serialBufferPtr = NULL;;
    LONGS_EQUAL(OK, SerialSpy_GetReceiveBuffer(serial, &serialBufferPtr));
    MEMCMP_EQUAL(expectedData, serialBufferPtr, SERIAL_RECEIVE_BUFFER_SIZE);
}
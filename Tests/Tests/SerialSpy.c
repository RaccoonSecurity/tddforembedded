#include "SerialSpy.h"
#include <string.h>


typedef struct SerialStruct
{
    char receiveBuffer[SERIAL_RECEIVE_BUFFER_SIZE];
    char sendBuffer[SERIAL_SEND_BUFFER_SIZE];
} SerialStruct;


Serial * Serial_Create(void)
{
    Serial * self = (Serial*)calloc(1, sizeof(SerialStruct));
    return self;
}

void Serial_Destroy(Serial * self)
{
    if (self == NULL)
    {
        return;
    }

    free(self);
    self = NULL;
}

static bool IsEndOfString(char * buffer)
{
    for (int i = 0; i < SERIAL_RECEIVE_BUFFER_SIZE; i++)
    {
        if (buffer[i] == '\n')
        {
            return true;
        }
    }
    return false;
}

Status Serial_ReceiveCommand(Serial * self, char * commandPtr)
{
    if (self == NULL || commandPtr == NULL)
    {
        return INVALID_PARAMETERS;
    }

    uint32_t commandLen = strlen(self->receiveBuffer);
    if (commandLen == 0)
    {
        return NO_DATA;
    }

    bool isEndOfString = IsEndOfString(self->receiveBuffer);
    if (isEndOfString == false)
    {
        return NO_DATA;
    }

    strncpy(commandPtr, self->receiveBuffer, commandLen);
    self->receiveBuffer[SERIAL_RECEIVE_BUFFER_SIZE - 1] = 0;
    return OK;
}

Status Serial_SendResponse(Serial * self, char * responsePtr)
{
    if (self == NULL || responsePtr == NULL)
    {
        return INVALID_PARAMETERS;
    }

    uint32_t responseLen = strlen(responsePtr);
    if (responseLen > SERIAL_SEND_BUFFER_SIZE)
    {
        return OUT_OF_BOUNDS;
    }

    strncpy(self->sendBuffer, responsePtr, responseLen);
    self->sendBuffer[SERIAL_SEND_BUFFER_SIZE - 1] = 0;
    return OK;
}


// Spy functions
Status SerialSpy_GetReceiveBuffer(Serial * self, char ** bufferPtr)
{
    if (self == NULL || bufferPtr == NULL)
    {
        return INVALID_PARAMETERS;
    }

    *bufferPtr = self->receiveBuffer;
    return OK;
}

Status SerialSpy_SetReceiveBuffer(Serial * self, char * data, uint32_t len)
{
    if (self == NULL || data == NULL)
    {
        return INVALID_PARAMETERS;
    }
    if (len > SERIAL_RECEIVE_BUFFER_SIZE)
    {
        return OUT_OF_BOUNDS;
    }

    memcpy(self->receiveBuffer, data, len);
    return OK;
}

Status SerialSpy_GetSendBuffer(Serial * self, char ** bufferPtr)
{
    if (self == NULL || bufferPtr == NULL)
    {
        return INVALID_PARAMETERS;
    }

    *bufferPtr = self->sendBuffer;
    return OK;
}

Status Serial_Clear(Serial * self)
{
    if (self == NULL)
    {
        return INVALID_PARAMETERS;
    }

    memset(self->receiveBuffer, 0, sizeof(self->receiveBuffer));
    return OK;
}
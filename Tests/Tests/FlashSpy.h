#pragma once

#include "Flash.h"

Status FlashSpy_GetFlashPtr(uint32_t ** flashMemoryPtr, uint32_t address);
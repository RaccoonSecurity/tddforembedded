#include <stdio.h>
#include <string.h>
#include "Configurator.h"
#include "Flash.h"


#define LEN_MIN_ARG         sizeof("0x0")
#define LEN_WRITE_COMMAND   sizeof(writeCommand) + LEN_MIN_ARG + LEN_MIN_ARG
#define LEN_READ_COMMAND    sizeof(readCommand) + LEN_MIN_ARG
#define LEN_ERASE_COMMAND   sizeof(eraseCommand) + LEN_MIN_ARG

typedef struct ConfiguratorStruct
{
    char command[SERIAL_RECEIVE_BUFFER_SIZE];
    Serial * serial;
} ConfiguratorStruct;


// Command list
static const char helpCommand[] = "help";
static const char writeCommand[] = "write:";
static const char readCommand[] = "read:";
static const char eraseCommand[] = "erase:";

// Response format list
static const char writeResponse[] = "Written successfully\r\n>";
static const char readResponseFormat[] = "Data: 0xFFFFFFFF\r\n>";
static const char eraseResponse[] = "Erased successfully\r\n>";
static const char unsupportedResponse [] = "Unsupported command received. Type 'help' to see commands.\r\n>";

static const char welcomeMessage[] = "Hello, User!\r\n>";


Configurator * Configurator_Create(Serial * serial)
{
    if (serial == NULL)
    {
        return NULL;
    }

    Configurator * self = (Configurator*)calloc(1, sizeof(ConfiguratorStruct));
    if (self == NULL)
    {
        return NULL;
    }

    self->serial = serial;
    Serial_SendResponse(self->serial, (char*)welcomeMessage);
    return self;
}

void Configurator_Destroy(Configurator * self)
{
    if (self == NULL)
    {
        return;
    }

    Serial_Destroy(self->serial);

    free(self);
    self = NULL;
}

static Status HandleHelpCommand(Configurator * self)
{
    if (self == NULL)
    {
        return INVALID_PARAMETERS;
    }

    Status status = Serial_SendResponse(self->serial, HELP_OUTPUT);
    return status;
}

static Status HandleWriteCommand(Configurator * self)
{
    if (self == NULL)
    {
        return INVALID_PARAMETERS;
    }
    
    // Check command length
    uint32_t commandLen = strlen(self->command);
    if (commandLen < LEN_WRITE_COMMAND)
    {
        return INVALID_PARAMETERS;
    }
    
    // Parse flash address to write
    char * flashAddressPtr = self->command + sizeof(writeCommand);
    uint32_t flashAddress = strtol(flashAddressPtr, (char**)NULL, 16);
    if (flashAddress > FLASH_SIZE)
    {
        return INVALID_PARAMETERS;
    }

    // Parse flash data to write
    char * dataAddressPtr = strchr(self->command + sizeof(writeCommand), ' ');
    uint32_t data = strtol(dataAddressPtr, (char**)NULL, 16);

    // Write to flash
    Status status = Flash_Write(flashAddress, data);
    if (status != OK)
    {
        return status;
    }

    // Send response vis Serial
    status = Serial_SendResponse(self->serial, (char*)writeResponse);
    return status;
}

static Status HandleReadCommand(Configurator * self)
{
    if (self == NULL)
    {
        return INVALID_PARAMETERS;
    }

    // Check command length
    uint32_t commandLen = strlen(self->command);
    if (commandLen < LEN_READ_COMMAND)
    {
        return INVALID_PARAMETERS;
    }

    // Parse flash address to read
    char * flashAddressPtr = self->command + sizeof(readCommand);
    uint32_t flashAddress = strtol(flashAddressPtr, (char**)NULL, 16);
    if (flashAddress > FLASH_SIZE)
    {
        return INVALID_PARAMETERS;
    }

    // Read flash data
    uint32_t data = 0;
    Status status = Flash_Read(flashAddress, &data);
    if (status != OK)
    {
        return status;
    }

    // Send response vis Serial
    char serialResponse[sizeof(readResponseFormat)] = "";
    sprintf(serialResponse, "%s: 0x%X\r\n>", "data", (int)data);
    status = Serial_SendResponse(self->serial, (char*)serialResponse);
    return status;
}

static Status HandleEraseCommand(Configurator * self)
{
    if (self == NULL)
    {
        return INVALID_PARAMETERS;
    }

    // Check command length
    uint32_t commandLen = strlen(self->command);
    if (commandLen < LEN_ERASE_COMMAND)
    {
        return INVALID_PARAMETERS;
    }

    // Parse flash page number to erase
    char * flashPageNumberPtr = self->command + sizeof(readCommand);
    uint32_t flashPageNumber = strtol(flashPageNumberPtr, (char**)NULL, 16);
    if (flashPageNumber >= FLASH_PAGE_COUNT)
    {
        return INVALID_PARAMETERS;
    }

    // Erase flash page
    Status status = Flash_Erase(flashPageNumber);
    if (status != OK)
    {
        return status;
    }

    // Send response vis Serial
    status = Serial_SendResponse(self->serial, (char*)eraseResponse);
    return status;
}

static Status HandleCommand(Configurator * self)
{
    if (self == NULL)
    {
        return INVALID_PARAMETERS;
    }

    if (strstr(self->command, helpCommand) != NULL)
    {
        return HandleHelpCommand(self);
    }
    else if (strstr(self->command, writeCommand) != NULL)
    {
        return HandleWriteCommand(self);
    } 
    else if (strstr(self->command, readCommand) != NULL)
    {
        return HandleReadCommand(self);
    }
    else if (strstr(self->command, eraseCommand) != NULL)
    {
        return HandleEraseCommand(self);
    }
    return UNSUPPORTED;
}

static Status Clear(Configurator * self)
{
    if (self == NULL)
    {
        return INVALID_PARAMETERS;
    }

    memset(self->command, 0, sizeof(self->command));
    Status status = Serial_Clear(self->serial);
    return status;
}

static Status HandleError(Configurator * self, Status inputStatus)
{
    if (self == NULL)
    {
        return INVALID_PARAMETERS;
    }

    if (inputStatus != UNSUPPORTED)
    {
        return inputStatus;
    }

    Status status = Serial_SendResponse(self->serial, (char*)unsupportedResponse);
    if (status != OK)
    {
        return status;
    }

    status = Clear(self);
    if (status != OK)
    {
        return status;
    }
    return inputStatus;
}

Status Configurator_Handler(Configurator * self)
{
    if (self == NULL)
    {
        return INVALID_PARAMETERS;
    }

    Status status = Serial_ReceiveCommand(self->serial, self->command);
    if (status != OK)
    {
        return status;
    }

    status = HandleCommand(self);
    if(HandleError(self, status) != OK)
    {
        return status;
    }

    status = Clear(self);
    return status;
}

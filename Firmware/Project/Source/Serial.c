#include "Serial.h"
#include <string.h>
#include "stm32f1xx_hal.h"


#define TIMEOUT_TRANSMIT    100


typedef struct SerialStruct
{
    char receiveBuffer[SERIAL_RECEIVE_BUFFER_SIZE];
    char sendBuffer[SERIAL_SEND_BUFFER_SIZE];
    UART_HandleTypeDef * huart;
    uint32_t receivePosition;
} SerialStruct;


extern UART_HandleTypeDef huart1;

Serial * serial = NULL;


Serial * Serial_Create(void)
{
    Serial * self = (Serial*)calloc(1, sizeof(SerialStruct));
    if (self == NULL)
    {
        return NULL;
    }

    self->huart = &huart1;
    serial = self;

    HAL_StatusTypeDef status = HAL_UART_Receive_IT(serial->huart, (uint8_t*)&serial->receiveBuffer[serial->receivePosition], 1);
    if(status != HAL_OK)
    {
        Serial_Destroy(self);
        return NULL;
    }

    return self;
}

void Serial_Destroy(Serial * self)
{
    if (self == NULL)
    {
        return;
    }

    free(self);
    self = NULL;
    serial = NULL;
}

static bool IsEndOfString(char * buffer)
{
    for (int i = 0; i < SERIAL_RECEIVE_BUFFER_SIZE; i++)
    {
        if (buffer[i] == '\n')
        {
            return true;
        }
    }
    return false;
}

void HAL_UART_RxCpltCallback(UART_HandleTypeDef *huart)
{
    if(&huart1 != huart)
    {
        return;
    }

    bool isEndOfStringReceived = serial->receiveBuffer[serial->receivePosition] == '\n';
    if(isEndOfStringReceived == true)
    {
        serial->receivePosition = 0;
    }

    bool isNotOutOfBounds = serial->receivePosition < (sizeof(serial->receiveBuffer) - 1);
    if(isNotOutOfBounds == true && isEndOfStringReceived == false)
    {
        serial->receivePosition++;
    }

    HAL_UART_Receive_IT(serial->huart, (uint8_t*)&serial->receiveBuffer[serial->receivePosition], 1);
}

Status Serial_ReceiveCommand(Serial * self, char * commandPtr)
{
    if (self == NULL || commandPtr == NULL)
    {
        return INVALID_PARAMETERS;
    }

    bool isEndOfString = IsEndOfString(self->receiveBuffer);
    if (isEndOfString == false)
    {
        return NO_DATA;
    }

    uint32_t receivedLen = strlen(self->receiveBuffer);
    strncpy(commandPtr, self->receiveBuffer, receivedLen);
    return OK;
}

Status Serial_SendResponse(Serial * self, char * responsePtr)
{
    if (self == NULL || responsePtr == NULL)
    {
        return INVALID_PARAMETERS;
    }

    uint32_t responseLen = strlen(responsePtr);
    if (responseLen > SERIAL_SEND_BUFFER_SIZE)
    {
        return OUT_OF_BOUNDS;
    }

    strncpy(self->sendBuffer, responsePtr, responseLen);
    self->sendBuffer[sizeof(self->sendBuffer) - 1] = 0;
    HAL_StatusTypeDef status = HAL_UART_Transmit(self->huart, (uint8_t*)self->sendBuffer, responseLen, TIMEOUT_TRANSMIT);
    if(status != HAL_OK)
    {
        return FAIL;
    }
    return OK;
}

Status Serial_Clear(Serial * self)
{
    if (self == NULL)
    {
        return INVALID_PARAMETERS;
    }

    memset(self->receiveBuffer, 0, sizeof(self->receiveBuffer));
    self->receivePosition = 0;
    return OK;
}

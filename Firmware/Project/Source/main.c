/* Default HAL includes */
#include "main.h"
#include "stm32f1xx_hal.h"
#include "usart.h"
#include "gpio.h"

/* User includes */
#include "Common.h"
#include "Flash.h"
#include "Configurator.h"


static void InitPeripheral(void)
{
    /* Reset of all peripherals, Initializes the Flash interface and the Systick. */
    HAL_Init();
    /* Configure the system clock */
    SystemClock_Config();
    /* Initialize all configured peripherals */
    MX_GPIO_Init();
    MX_USART1_UART_Init();
}

int main(void)
{
    InitPeripheral();

    Status status = Flash_Init();
    if(status != OK)
    {
        Error_Handler();
    }

    Serial * serial = Serial_Create();
    if(serial == NULL)
    {
        Error_Handler();
    }

    Configurator * configurator = Configurator_Create(serial);
    if(serial == NULL)
    {
        Error_Handler();
    }

    while (1)
    {
        status = Configurator_Handler(configurator);
        if(status != OK && status != NO_DATA && status != UNSUPPORTED)
        {
            Error_Handler();
        }
    }
}



#include "stm32f1xx_hal.h"
#include "Flash.h"
#include <string.h>

#define FLASH_START_OFFSET 0x08000000

Status Flash_Init(void)
{
    HAL_StatusTypeDef halStatus = HAL_FLASH_Unlock();
    if(halStatus != HAL_OK)
    {
        return FAIL;
    }
    return OK;
}

Status Flash_DeInit(void)
{
    HAL_StatusTypeDef halStatus = HAL_FLASH_Lock();
    if(halStatus != HAL_OK)
    {
        return FAIL;
    }
    return OK;
}

Status Flash_Write(uint32_t address, uint32_t data)
{
    if (address >= FLASH_SIZE)
    {
        return INVALID_PARAMETERS;
    }

    uint32_t offset = FLASH_START_OFFSET + address;
    HAL_StatusTypeDef halStatus = HAL_FLASH_Program(FLASH_TYPEPROGRAM_WORD, offset, data);
    if(halStatus != HAL_OK)
    {
        return FAIL;
    }
    return OK;
}

Status Flash_Read(uint32_t address, uint32_t * dataPtr)
{
    if (dataPtr == NULL)
    {
        return INVALID_PARAMETERS;
    }
    if (address >= FLASH_SIZE)
    {
        return INVALID_PARAMETERS;
    }

    *dataPtr = *((volatile uint32_t*)(FLASH_START_OFFSET + address));
    return OK;
}

Status Flash_Erase(uint8_t pageNumber)
{
    if (pageNumber >= FLASH_PAGE_COUNT)
    {
        return INVALID_PARAMETERS;
    }

    HAL_StatusTypeDef status;
    uint32_t pageError;

    FLASH_EraseInitTypeDef eraseInit;
    eraseInit.TypeErase   = FLASH_TYPEERASE_PAGES;
    eraseInit.PageAddress = pageNumber * FLASH_PAGE_SIZE + FLASH_START_OFFSET;
    eraseInit.NbPages     = 1;

    status = HAL_FLASHEx_Erase(&eraseInit, &pageError);
    if(status != HAL_OK)
    {
        return FAIL;
    }

    return OK;
}

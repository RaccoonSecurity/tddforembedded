#pragma once

#include "Common.h"
#include "Serial.h"


#define HELP_OUTPUT \
"Command list:\r\n \
    - help\r\n \
    - read: <flash_address_in_hex>\r\n \
    - write: <flash_address_in_hex> <data_to_write>\r\n \
    - erase: <flash_page_number_to_erase>\r\n>"

typedef struct ConfiguratorStruct Configurator;


Configurator * Configurator_Create(Serial * serial);

void Configurator_Destroy(Configurator * self);

Status Configurator_Handler(Configurator * self);

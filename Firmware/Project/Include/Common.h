#pragma once

#include <stdlib.h>
#include <stdint.h>
#include <stdbool.h>


typedef enum
{
    OK = 0,
    FAIL = -1,
    NOT_IMPLEMENTED = -2,
    INVALID_PARAMETERS = -4,
    NOT_FOUND = -6,
    INVALID_SIGNATURE = -7,
    UNSUPPORTED = -9,
    OUT_OF_BOUNDS = -13,
    NO_DATA = -18,
    WRONG_CONDITION = -19,
    INVALID_CHECKSUM = -20,
    SIZE_MISMATCH = -21, 
    TIME_OUT = -22,      
    BUSY = -23,
} Status;

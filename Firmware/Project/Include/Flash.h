#pragma once

#include "Common.h"

#define FLASH_PAGE_COUNT    0x80
#ifndef FLASH_PAGE_SIZE
#define FLASH_PAGE_SIZE     0x400
#endif
#define FLASH_SIZE          FLASH_PAGE_COUNT * FLASH_PAGE_SIZE     // 128 Kbyte

Status Flash_Init(void);

Status Flash_DeInit(void);

Status Flash_Write(uint32_t address, uint32_t data);

Status Flash_Read(uint32_t address, uint32_t * dataPtr);

Status Flash_Erase(uint8_t pageNumber);

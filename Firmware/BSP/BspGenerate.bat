REM Set Stm32CubeMX path
SET _Stm32CubeMXExe=C:\Program Files\STMicroelectronics\STM32Cube\STM32CubeMX\STM32CubeMX.exe

REM Go to BspGenerate.bat directory 
cd /D %~dp0%

REM Generate project
java -jar "%_Stm32CubeMXExe%" -q BspGenerate.scr

REM Generate SystemClock.c from main.c
move /Y .\Src\main.c .\Src\SystemClock.c 
